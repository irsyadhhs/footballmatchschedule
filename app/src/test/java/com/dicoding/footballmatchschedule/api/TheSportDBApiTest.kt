package com.dicoding.footballmatchschedule.api

import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test

class TheSportDBApiTest {

    private lateinit var theSportDBApi: TheSportDBApi

    @Test
    fun testGetLastMatch() {
        val league = "English Premiere League"
        val actualUrl = theSportDBApi.getLastMatch(league)
        val expectedUrl = "https://www.thesportsdb.com/api/v1/json/1/search_all_teams.php?l=English%20Premier%20League"
        assertEquals(expectedUrl, actualUrl)
    }

    @Test
    fun getNextMatch() {
    }

    @Test
    fun getTeams() {
    }

    @Test
    fun getEventDetail() {
    }

    @Before
    fun setUp() {
        //MockitoAnnotations.initMocks(this)
        theSportDBApi = TheSportDBApi
    }
}