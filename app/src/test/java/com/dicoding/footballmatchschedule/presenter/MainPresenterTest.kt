package com.dicoding.footballmatchschedule.presenter

import com.dicoding.footballmatchschedule.api.ApiRepository
import com.dicoding.footballmatchschedule.api.TheSportDBApi
import com.dicoding.footballmatchschedule.model.MainModel
import com.dicoding.footballmatchschedule.model.MainResponseModel
import com.dicoding.footballmatchschedule.utils.TestContextProvider
import com.dicoding.footballmatchschedule.view.MainView
import com.google.gson.Gson
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import org.junit.Before
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.Mockito.`when`
import org.mockito.MockitoAnnotations

class MainPresenterTest {
    private lateinit var presenter: MainPresenter

    @Mock
    private
    lateinit var view: MainView

    @Mock
    private
    lateinit var gson: Gson

    @Mock
    private
    lateinit var apiRepository: ApiRepository

    @Before
    fun setUp() {
        MockitoAnnotations.initMocks(this)
        presenter = MainPresenter(view, apiRepository, gson, TestContextProvider())
    }

    @Test
    fun getLastMatch() {
        val match: MutableList<MainModel> = mutableListOf()
        val response = MainResponseModel(match)
        val league = "English Premiere League"

        GlobalScope.launch {
            `when`(gson.fromJson(apiRepository
                    .doRequest(TheSportDBApi.getTeams(league)).await(),
                    MainResponseModel::class.java
            )).thenReturn(response)

            presenter.getLastMatch(league)

            Mockito.verify(view).showLoading()
            Mockito.verify(view).showTeamList(match)
            Mockito.verify(view).hideLoading()
        }
    }

    @Test
    fun getNexttMatch() {
        val match: MutableList<MainModel> = mutableListOf()
        val response = MainResponseModel(match)
        val league = "English Premiere League"

        GlobalScope.launch {
            `when`(gson.fromJson(apiRepository
                    .doRequest(TheSportDBApi.getTeams(league)).await(),
                    MainResponseModel::class.java
            )).thenReturn(response)

            presenter.getNexttMatch(league)

            Mockito.verify(view).showLoading()
            Mockito.verify(view).showTeamList(match)
            Mockito.verify(view).hideLoading()
        }
    }
}