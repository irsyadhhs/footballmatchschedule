package com.dicoding.footballmatchschedule.database

import android.content.Context
import android.database.sqlite.SQLiteDatabase
import com.dicoding.footballmatchschedule.model.MatchFavorite
import org.jetbrains.anko.db.*

class MyDatabaseOpenHelper(ctx: Context) : ManagedSQLiteOpenHelper(ctx, "FavoriteMatch.db", null, 1) {
    companion object {
        private var instance: MyDatabaseOpenHelper? = null

        @Synchronized
        fun getInstance(ctx: Context): MyDatabaseOpenHelper {
            if (instance == null) {
                instance = MyDatabaseOpenHelper(ctx.applicationContext)
            }
            return instance as MyDatabaseOpenHelper
        }
    }

    override fun onCreate(db: SQLiteDatabase) {
        db.createTable(MatchFavorite.TABLE_FAVORITE, true,
                MatchFavorite.ID to INTEGER + PRIMARY_KEY + AUTOINCREMENT,
                MatchFavorite.EVENT_ID to TEXT + UNIQUE,
                MatchFavorite.EVENT_NAME to TEXT,
                MatchFavorite.EVENT_DATE to TEXT,
                MatchFavorite.TEAM_HOME_SCORE to TEXT,
                MatchFavorite.TEAM_AWAY_SCORE to TEXT)
    }

    override fun onUpgrade(db: SQLiteDatabase, oldVersion: Int, newVersion: Int) {
        db.dropTable(MatchFavorite.TABLE_FAVORITE, true)
    }
}

// Access property for Context
val Context.database: MyDatabaseOpenHelper
    get() = MyDatabaseOpenHelper.getInstance(applicationContext)