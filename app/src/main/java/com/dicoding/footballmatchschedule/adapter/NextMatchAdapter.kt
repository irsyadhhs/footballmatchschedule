package com.dicoding.footballmatchschedule.adapter

import android.content.Context
import androidx.recyclerview.widget.RecyclerView
import android.text.format.DateFormat
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.dicoding.footballmatchschedule.R
import com.dicoding.footballmatchschedule.model.MainModel
import org.jetbrains.anko.find
import java.text.SimpleDateFormat
import java.util.*

class NextMatchAdapter(private val context: Context?, private val items: List<MainModel>, private val listener: (MainModel) -> Unit)
    : androidx.recyclerview.widget.RecyclerView.Adapter<NextMatchAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
            ViewHolder(LayoutInflater.from(context).inflate(R.layout.item_match, parent, false))

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bindItem(items[position], listener)
    }

    override fun getItemCount(): Int = items.size

    class ViewHolder(view: View) : androidx.recyclerview.widget.RecyclerView.ViewHolder(view) {

        private val day: TextView = view.find(R.id.day)
        private val date: TextView = view.find(R.id.date)
        private val month: TextView = view.find(R.id.month)
        private val teamName1: TextView = view.find(R.id.team1)
        private val teamName2: TextView = view.find(R.id.team2)
        private val teamScore1: TextView = view.find(R.id.score1)
        private val teamScore2: TextView = view.find(R.id.score2)

        fun bindItem(items: MainModel, listener: (MainModel) -> Unit) {
            day.text = getParsing("EEE", items.time)
            date.text = getParsing("dd", items.time)
            month.text = getParsing("MMM", items.time)
            teamName1.text = items.event?.split(" vs ")?.get(0)
            teamName2.text = items.event?.split(" vs ")?.get(1)
            teamScore1.text = "-"
            teamScore2.text = "-"


            itemView.setOnClickListener {
                listener(items)
            }
        }

        fun getParsing(parser: String, dateString: String?): String? {
            val format = SimpleDateFormat("yyyy-MM-dd", Locale.US)
            val date = format.parse(dateString)
            return DateFormat.format(parser, date) as String
        }
    }
}

