package com.dicoding.footballmatchschedule.adapter

import android.content.Context
import androidx.recyclerview.widget.RecyclerView
import android.text.format.DateFormat
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.dicoding.footballmatchschedule.R
import com.dicoding.footballmatchschedule.model.MatchFavorite
import org.jetbrains.anko.find
import java.text.SimpleDateFormat
import java.util.*

class FavoriteAdapter(private val context: Context?, private val items: List<MatchFavorite>, private val listener: (MatchFavorite) -> Unit)
    : androidx.recyclerview.widget.RecyclerView.Adapter<FavoriteAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
            ViewHolder(LayoutInflater.from(context).inflate(R.layout.item_match, parent, false))

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bindItem(items[position], listener)
    }

    override fun getItemCount(): Int = items.size

    class ViewHolder(view: View) : androidx.recyclerview.widget.RecyclerView.ViewHolder(view) {

        private val teamName1: TextView = view.find(R.id.team1)
        private val teamName2: TextView = view.find(R.id.team2)
        private val teamScore1: TextView = view.find(R.id.score1)
        private val teamScore2: TextView = view.find(R.id.score2)
        private val day: TextView = view.find(R.id.day)
        private val date: TextView = view.find(R.id.date)
        private val month: TextView = view.find(R.id.month)

        fun bindItem(items: MatchFavorite, listener: (MatchFavorite) -> Unit) {
            day.text = getParsing("EEE", items.eventDate)
            date.text = getParsing("dd", items.eventDate)
            month.text = getParsing("MMM", items.eventDate)
            teamName1.text = items.eventName?.split(" vs ")?.get(0)
            teamName2.text = items.eventName?.split(" vs ")?.get(1)
            teamScore1.text = items.teamHomeScore
            teamScore2.text = items.teamAwayScore

            itemView.setOnClickListener {
                listener(items)
            }
        }

        fun getParsing(parser: String, dateString: String?): String? {
            val format = SimpleDateFormat("yyyy-MM-dd", Locale.US)
            val date = format.parse(dateString)
            return DateFormat.format(parser, date) as String
        }
    }
}