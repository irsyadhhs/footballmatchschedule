package com.dicoding.footballmatchschedule.presenter

import com.dicoding.footballmatchschedule.api.ApiRepository
import com.dicoding.footballmatchschedule.api.TheSportDBApi
import com.dicoding.footballmatchschedule.model.MainResponseModel
import com.dicoding.footballmatchschedule.utils.CoroutineContextProvider
import com.dicoding.footballmatchschedule.view.MainView
import com.google.gson.Gson
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

class MainPresenter(private val view: MainView,
                    private val apiRepository: ApiRepository,
                    private val gson: Gson,
                    private val context: CoroutineContextProvider = CoroutineContextProvider()) {

    fun getLastMatch(league: String?) {
        view.showLoading()
        GlobalScope.launch(context.main) {
            val data = gson.fromJson(apiRepository
                    .doRequest(TheSportDBApi.getLastMatch(league)).await(),
                    MainResponseModel::class.java)

            view.hideLoading()
            view.showTeamList(data.events)

        }
    }

    fun getNexttMatch(league: String?) {
        view.showLoading()
        GlobalScope.launch(context.main) {
            val data = gson.fromJson(apiRepository
                    .doRequest(TheSportDBApi.getNextMatch(league)).await(),
                    MainResponseModel::class.java)

            view.hideLoading()
            view.showTeamList(data.events)
        }
    }

    /*fun getNexttMatch(league: String?) {
        view.showLoading()
        doAsync {
            val data = gson.fromJson(apiRepository
                    .doRequest(TheSportDBApi.getNextMatch(league)),
                    MainResponseModel::class.java
            )

            uiThread {
                view.hideLoading()
                view.showTeamList(data.events)
            }
        }
    }*/
}