package com.dicoding.footballmatchschedule.presenter

import com.dicoding.footballmatchschedule.api.ApiRepository
import com.dicoding.footballmatchschedule.api.TheSportDBApi
import com.dicoding.footballmatchschedule.model.TeamResponseModel
import com.dicoding.footballmatchschedule.view.TeamBadgeView
import com.google.gson.Gson
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

class TeamBadgePresenter(private val view: TeamBadgeView,
                         private val apiRepository: ApiRepository,
                         private val gson: Gson) {

    fun getTeamHomeBadge(league: String?) {
        GlobalScope.launch(Dispatchers.Main) {
            val data = gson.fromJson(apiRepository
                    .doRequest(TheSportDBApi.getTeams(league)).await(),
                    TeamResponseModel::class.java)

            view.showTeamHomeBadge(data.teams)
        }
    }

    fun getTeamAwayBadge(league: String?) {
        GlobalScope.launch(Dispatchers.Main) {
            val data = gson.fromJson(apiRepository
                    .doRequest(TheSportDBApi.getTeams(league)).await(),
                    TeamResponseModel::class.java)

            view.showTeamAwayBadge(data.teams)

        }
    }
}