package com.dicoding.footballmatchschedule.activity

import android.app.Activity
import android.database.sqlite.SQLiteConstraintException
import android.os.Bundle
import androidx.core.content.ContextCompat
import androidx.appcompat.app.AppCompatActivity
import android.text.format.DateFormat
import android.view.Menu
import android.view.MenuItem
import android.widget.TextView
import com.dicoding.footballmatchschedule.R
import com.dicoding.footballmatchschedule.R.drawable.outline_favorite_24
import com.dicoding.footballmatchschedule.R.drawable.outline_favorite_border_24
import com.dicoding.footballmatchschedule.R.id.add_to_favorite
import com.dicoding.footballmatchschedule.R.menu.detail_menu
import com.dicoding.footballmatchschedule.api.ApiRepository
import com.dicoding.footballmatchschedule.api.TheSportDBApi
import com.dicoding.footballmatchschedule.database.database
import com.dicoding.footballmatchschedule.model.MainModel
import com.dicoding.footballmatchschedule.model.MainResponseModel
import com.dicoding.footballmatchschedule.model.MatchFavorite
import com.dicoding.footballmatchschedule.model.TeamModel
import com.dicoding.footballmatchschedule.presenter.TeamBadgePresenter
import com.dicoding.footballmatchschedule.view.TeamBadgeView
import com.google.gson.Gson
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_event_detail.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import org.jetbrains.anko.db.classParser
import org.jetbrains.anko.db.delete
import org.jetbrains.anko.db.insert
import org.jetbrains.anko.db.select
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.toast
import org.jetbrains.anko.uiThread
import java.text.SimpleDateFormat
import java.util.*

class EventDetailActivity : AppCompatActivity(), TeamBadgeView {

    private lateinit var eventDetail: MainModel
    private lateinit var eventDetailFav: MatchFavorite
    private lateinit var id: String

    private var goalHome: List<String>? = mutableListOf()
    private var goalAway: List<String>? = mutableListOf()
    private var goalKeeperHome: List<String>? = mutableListOf()
    private var goalKeeperAway: List<String>? = mutableListOf()
    private var defenseHome: List<String>? = mutableListOf()
    private var defenseAway: List<String>? = mutableListOf()
    private var midfieldHome: List<String>? = mutableListOf()
    private var midfieldAway: List<String>? = mutableListOf()
    private var forwardHome: List<String>? = mutableListOf()
    private var forwardAway: List<String>? = mutableListOf()
    private var substituteHome: List<String>? = mutableListOf()
    private var substituteAway: List<String>? = mutableListOf()

    private lateinit var presenter: TeamBadgePresenter
    private var isFavorite: Boolean = false

    private var menuItem: Menu? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_event_detail)

        supportActionBar?.title = getString(R.string.title_match_detail)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        if (intent.getSerializableExtra("mode").equals("favorite")) {
            eventDetailFav = intent.getSerializableExtra("detail") as MatchFavorite
            id = eventDetailFav.eventId ?: ""

            val request = ApiRepository()
            val gson = Gson()
            presenter = TeamBadgePresenter(this, request, gson)
            presenter.getTeamHomeBadge(eventDetailFav.eventName?.split(" vs ")?.get(0))
            presenter.getTeamAwayBadge(eventDetailFav.eventName?.split(" vs ")?.get(1))

            team_name1.text = eventDetailFav.eventName?.split(" vs ")?.get(0)
            team_name2.text = eventDetailFav.eventName?.split(" vs ")?.get(1)
            team_score1.text = eventDetailFav.teamHomeScore
            team_score2.text = eventDetailFav.teamAwayScore

            time.text = getParsing("EEE, dd MMMM yyyy", eventDetailFav.eventDate)
            loadFromFavorite(eventDetailFav)
        } else {
            eventDetail = intent.getSerializableExtra("detail") as MainModel
            id = eventDetail.idEvent ?: ""

            val request = ApiRepository()
            val gson = Gson()
            presenter = TeamBadgePresenter(this, request, gson)
            presenter.getTeamHomeBadge(eventDetail.event?.split(" vs ")?.get(0))
            presenter.getTeamAwayBadge(eventDetail.event?.split(" vs ")?.get(1))

            team_name1.text = eventDetail.event?.split(" vs ")?.get(0)
            team_name2.text = eventDetail.event?.split(" vs ")?.get(1)
            team_score1.text = eventDetail.score1
            team_score2.text = eventDetail.score2
            shot_number1.text = eventDetail.shot1
            shot_number2.text = eventDetail.shot2
            time.text = getParsing("EEE, dd MMMM yyyy", eventDetail.time)

            loadItem(eventDetail)
            loadTextView()
        }
    }

    private fun loadItem(event: MainModel) {
        goalHome = event.goalDetail1?.split(";")
        goalAway = event.goalDetail2?.split(";")
        goalKeeperHome = event.lineupGoalKeeper1?.split(";")
        goalKeeperAway = event.lineupGoalKeeper2?.split(";")
        defenseHome = event.lineupDefense1?.split(";")
        defenseAway = event.lineupDefense2?.split(";")
        midfieldHome = event.lineupMidfield1?.split(";")
        midfieldAway = event.lineupMidfield2?.split(";")
        forwardHome = event.lineupForward1?.split(";")
        forwardAway = event.lineupForward2?.split(";")
        substituteHome = event.lineupSubstitute1?.split(";")
        substituteAway = event.lineupSubstitute2?.split(";")
    }

    private fun loadTextView() {
        setTextview(goalHome, goal_list1)
        setTextview(goalAway, goal_list2)
        setTextview(goalKeeperHome, goal_keeper1)
        setTextview(goalKeeperAway, goal_keeper2)
        setTextview(defenseHome, defense1)
        setTextview(defenseAway, defense2)
        setTextview(midfieldHome, midfield1)
        setTextview(midfieldAway, midfield2)
        setTextview(forwardHome, forward1)
        setTextview(forwardAway, forward2)
        setTextview(substituteHome, substitute1)
        setTextview(substituteAway, substitute2)
    }

    private fun setTextview(list: List<String>?, view: TextView) {
        val max = list?.size ?: 1
        for (i in 0..max - 1) {
            val str = list?.get(i) ?: ""
            if (str.length > 15) {
                view.text = view.text.toString() + "\n" + str.substring(0, 15) + ".."
            } else {
                view.text = view.text.toString() + "\n" + str
            }
        }
    }

    private fun getParsing(parser: String, dateString: String?): String? {
        val format = SimpleDateFormat("yyyy-MM-dd", Locale.US)
        val date = format.parse(dateString)
        return DateFormat.format(parser, date) as String
    }

    private fun setFavorite() {
        if (isFavorite)
            menuItem?.getItem(0)?.icon = ContextCompat.getDrawable(this, outline_favorite_24)
        else
            menuItem?.getItem(0)?.icon = ContextCompat.getDrawable(this, outline_favorite_border_24)
    }

    private fun checkFavorite() {
        database.use {
            val result = select(MatchFavorite.TABLE_FAVORITE)
                    .whereArgs(MatchFavorite.EVENT_ID + " = {id}",
                            "id" to id)
            val favorite = result.parseList(classParser<MatchFavorite>())
            if (!favorite.isEmpty()) {
                isFavorite = true
            }
            setFavorite()
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(detail_menu, menu)
        menuItem = menu
        checkFavorite()
        return true
    }

    fun addToFavorite() {
        try {
            database.use {
                insert(MatchFavorite.TABLE_FAVORITE,
                        MatchFavorite.EVENT_ID to eventDetail.idEvent,
                        MatchFavorite.EVENT_NAME to eventDetail.event,
                        MatchFavorite.EVENT_DATE to eventDetail.time,
                        MatchFavorite.TEAM_HOME_SCORE to eventDetail.score1,
                        MatchFavorite.TEAM_AWAY_SCORE to eventDetail.score2)
            }
            toast(getString(R.string.success_add_fav))
        } catch (e: SQLiteConstraintException) {
            toast(getString(R.string.failed_add_fav))
        }
    }

    private fun removeFromFavorite() {
        try {
            database.use {
                delete(MatchFavorite.TABLE_FAVORITE, MatchFavorite.EVENT_ID + " = {id}",
                        "id" to id)
            }
            toast(getString(R.string.success_remove_fav))
            setResult(Activity.RESULT_OK)
        } catch (e: SQLiteConstraintException) {
            toast(getString(R.string.failed_remove_fav))
        }
    }

    private fun loadFromFavorite(favorite: MatchFavorite) {
        GlobalScope.launch(Dispatchers.Main) {
            val data = Gson().fromJson(ApiRepository()
                    .doRequest(TheSportDBApi.getEventDetail(favorite.eventId)).await(),
                    MainResponseModel::class.java
            )


            loadItem(data.events.get(0))
            loadTextView()
            shot_number1.text = data.events[0].shot1
            shot_number2.text = data.events[0].shot2
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            android.R.id.home -> {
                finish()
                true
            }
            add_to_favorite -> {
                if (isFavorite) removeFromFavorite() else addToFavorite()

                isFavorite = !isFavorite
                setFavorite()
                true
            }

            else -> super.onOptionsItemSelected(item)
        }
    }

    override fun showTeamHomeBadge(data: List<TeamModel>) {
        Picasso.get().load(data[0].teamBadge).into(team_logo1)
    }

    override fun showTeamAwayBadge(data: List<TeamModel>) {
        Picasso.get().load(data[0].teamBadge).into(team_logo2)
    }
}
