package com.dicoding.footballmatchschedule.fragment


import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ProgressBar
import com.dicoding.footballmatchschedule.R
import com.dicoding.footballmatchschedule.activity.EventDetailActivity
import com.dicoding.footballmatchschedule.adapter.LastMatchAdapter
import com.dicoding.footballmatchschedule.api.ApiRepository
import com.dicoding.footballmatchschedule.model.MainModel
import com.dicoding.footballmatchschedule.presenter.MainPresenter
import com.dicoding.footballmatchschedule.utils.invisible
import com.dicoding.footballmatchschedule.utils.visible
import com.dicoding.footballmatchschedule.view.MainView
import com.google.gson.Gson
import org.jetbrains.anko.support.v4.startActivity

class LastMatchFragment : androidx.fragment.app.Fragment(), MainView {

    private var items: MutableList<MainModel> = mutableListOf()

    private lateinit var recyclerview: androidx.recyclerview.widget.RecyclerView
    private lateinit var progress: ProgressBar

    private lateinit var presenter: MainPresenter
    private lateinit var adapter: LastMatchAdapter

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {

        val view: View = inflater.inflate(R.layout.fragment_last_match, container, false)

        recyclerview = view.findViewById(R.id.match_list)
        progress = view.findViewById(R.id.progressBar)

        adapter = LastMatchAdapter(context, items) {
            //startActivity<EventDetailActivity>("detail" to it, "mode" to "last")
        }

        recyclerview.layoutManager = androidx.recyclerview.widget.LinearLayoutManager(context)
        recyclerview.adapter = adapter


        val request = ApiRepository()
        val gson = Gson()
        presenter = MainPresenter(this, request, gson)
        presenter.getLastMatch("4328")

        // Inflate the layout for this fragment
        return view
    }

    override fun showLoading() {
        progress.visible()
    }

    override fun hideLoading() {
        progress.invisible()
    }

    override fun showTeamList(data: List<MainModel>) {
        items.clear()
        items.addAll(data)
        adapter.notifyDataSetChanged()
    }

}
