package com.dicoding.footballmatchschedule.fragment

import android.app.Activity.RESULT_OK
import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.dicoding.footballmatchschedule.R
import com.dicoding.footballmatchschedule.activity.EventDetailActivity
import com.dicoding.footballmatchschedule.adapter.FavoriteAdapter
import com.dicoding.footballmatchschedule.database.database
import com.dicoding.footballmatchschedule.model.MatchFavorite
import org.jetbrains.anko.db.classParser
import org.jetbrains.anko.db.select
import org.jetbrains.anko.support.v4.startActivity

class FavoriteFragment : androidx.fragment.app.Fragment() {

    private var items: MutableList<MatchFavorite> = mutableListOf()
    private var REQ_CODE = 123

    private lateinit var recyclerview: androidx.recyclerview.widget.RecyclerView
    private lateinit var adapter: FavoriteAdapter

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        val view : View = inflater.inflate(R.layout.fragment_favorite, container, false)

        recyclerview = view.findViewById(R.id.match_list)
        adapter = FavoriteAdapter(context, items){
            //startActivity<EventDetailActivity>("detail" to it, "mode" to "favorite")
            val intent = Intent(context, EventDetailActivity::class.java)
            intent.putExtra("mode", "favorite")
            intent.putExtra("detail", it)
            startActivityForResult(intent, REQ_CODE)
        }

        recyclerview.layoutManager = androidx.recyclerview.widget.LinearLayoutManager(context)
        recyclerview.adapter = adapter

        showFavorite()

        return view
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if((requestCode == REQ_CODE) && (resultCode == RESULT_OK)){
            showFavorite()
        }
    }

    private fun showFavorite(){
        items.clear()
        context?.database?.use {
            val result = select(MatchFavorite.TABLE_FAVORITE)
            val favorite = result.parseList(classParser<MatchFavorite>())
            items.addAll(favorite)
            adapter.notifyDataSetChanged()
        }
    }
}
