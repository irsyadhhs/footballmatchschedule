package com.dicoding.footballmatchschedule.model

import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class MainModel(@SerializedName("idEvent")
                     val idEvent: String?,
                     @SerializedName("strEvent")
                     val event: String?,
                     @SerializedName("intHomeScore")
                     val score1: String?,
                     @SerializedName("intAwayScore")
                     val score2: String?,
                     @SerializedName("dateEvent")
                     val time: String?,
                     @SerializedName("intHomeShots")
                     val shot1: String?,
                     @SerializedName("intAwayShots")
                     val shot2: String?,
                     @SerializedName("strHomeGoalDetails")
                     val goalDetail1: String?,
                     @SerializedName("strHomeLineupGoalkeeper")
                     val lineupGoalKeeper1: String?,
                     @SerializedName("strHomeLineupDefense")
                     val lineupDefense1: String?,
                     @SerializedName("strHomeLineupMidfield")
                     val lineupMidfield1: String?,
                     @SerializedName("strHomeLineupForward")
                     val lineupForward1: String?,
                     @SerializedName("strHomeLineupSubstitutes")
                     val lineupSubstitute1: String?,
                     @SerializedName("strAwayGoalDetails")
                     val goalDetail2: String?,
                     @SerializedName("strAwayLineupGoalkeeper")
                     val lineupGoalKeeper2: String?,
                     @SerializedName("strAwayLineupDefense")
                     val lineupDefense2: String?,
                     @SerializedName("strAwayLineupMidfield")
                     val lineupMidfield2: String?,
                     @SerializedName("strAwayLineupForward")
                     val lineupForward2: String?,
                     @SerializedName("strAwayLineupSubstitutes")
                     val lineupSubstitute2: String?) : Serializable






