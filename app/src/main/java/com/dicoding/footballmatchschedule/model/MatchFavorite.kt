package com.dicoding.footballmatchschedule.model

import java.io.Serializable

data class MatchFavorite(val id: Long?,
                         val eventId: String?,
                         val eventName: String?,
                         val eventDate: String?,
                         val teamHomeScore: String?,
                         val teamAwayScore: String?) : Serializable {
    companion object {
        const val TABLE_FAVORITE: String = "TABLE_FAVORITE"
        const val ID: String = "ID_"
        const val EVENT_ID: String = "EVENT_ID"
        const val EVENT_NAME: String = "EVENT_NAME"
        const val EVENT_DATE: String = "EVENT_DATE"
        const val TEAM_HOME_SCORE: String = "TEAM_HOME_SCORE"
        const val TEAM_AWAY_SCORE: String = "TEAM_AWAY_SCORE"
    }
}
