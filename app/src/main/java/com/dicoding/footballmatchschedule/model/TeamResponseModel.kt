package com.dicoding.footballmatchschedule.model

data class TeamResponseModel(
        val teams: List<TeamModel>)