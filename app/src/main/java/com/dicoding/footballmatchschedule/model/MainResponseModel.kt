package com.dicoding.footballmatchschedule.model

data class MainResponseModel(
        val events: List<MainModel>)