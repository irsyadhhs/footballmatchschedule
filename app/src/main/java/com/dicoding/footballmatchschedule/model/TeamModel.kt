package com.dicoding.footballmatchschedule.model

import com.google.gson.annotations.SerializedName

data class TeamModel(@SerializedName("idTeam")
                     var teamId: String? = null,

                     @SerializedName("strTeam")
                     var teamName: String? = null,

                     @SerializedName("strTeamBadge")
                     var teamBadge: String? = null)