package com.dicoding.footballmatchschedule.view

import com.dicoding.footballmatchschedule.model.TeamModel

interface TeamBadgeView {
    fun showTeamHomeBadge(data: List<TeamModel>)
    fun showTeamAwayBadge(data: List<TeamModel>)
}