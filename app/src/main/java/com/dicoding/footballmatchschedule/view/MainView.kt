package com.dicoding.footballmatchschedule.view

import com.dicoding.footballmatchschedule.model.MainModel

interface MainView {
    fun showLoading()
    fun hideLoading()
    fun showTeamList(data: List<MainModel>)
}